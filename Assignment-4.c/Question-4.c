#include<stdio.h>  
 int main()    
{    
	// program to convert temperature given in celsius to Fahrenheit
	
	float celsius, fahrenheit;
	
	printf("Enter temperature in Celsius : ");
	scanf("%f", &celsius);
	
	//calculation
	fahrenheit = (celsius*9/5) +32;
	
	printf("Temperature in fahrenheit = %.2fF",fahrenheit); 
	  
	return 0;  
}   
