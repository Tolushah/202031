#include<stdio.h>
int main()
{
	//program to swap two numbers without using a temporary third variable
		int x, y;
		
		printf("Input value for x & y: \n");
		scanf("%d %d",&x,&y);
		printf("Before swapping the value of x = %d y = %d",x,y);
		
		x=x+y; 
		y=x-y;
		x=x-y;
		
		printf("\nAfter swapping the value of x = %d y = %d",x,y);
		return 0;
}
