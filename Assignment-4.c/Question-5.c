#include <stdio.h>
int binary(int);

int main(){
	 // program to demonstrate bitwise opeartors and values
	 
	  unsigned int A=10, B=15, a=0, b=0, c=0, d=0, e=0;
	  //bitwise operators
	  
	  printf("%d = %d\t %d = %d\n\n",A,binary(A),B,binary(B));
	  
	  a= A&B;
	  printf("%d & %d = %d\n",A,B,a); 
	  printf("A&B = %d \n\n",binary(a));
	  
	  b= A^B;
	  printf("%d ^ %d = %d\n",A,B,b);
	  printf("A^B = %d \n\n",binary(b));
	  
	  c= ~A;
	  printf("~%d = %d\n",A,c);
	  printf("~A = %d\n\n", binary(c));
	  
	  d= A<<3;
	  printf("%d<<3 = %d\n",A,d);
	  printf("A<<3 = %d\n\n", binary(d));
	   
	  e= B>>3;
	   printf("%d>>3 = %d\n",B,e);
	   printf("%B>>3 = %d\n",binary(e));
	  
	  return 0;
}

int binary(int n){
	if (n==0)
	return 0;
	else return (n%2)+10*binary(n/2);
}
