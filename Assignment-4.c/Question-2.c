#include <stdio.h>
int main(){
	
	//program to find the volume of a cone using the radius & height given by the user
	
	float radius, height, volume;
	const double PI = 3.14;
	
	printf("Enter the radius of the cone : ");
	scanf("%f", &radius);
	
	printf("Enter the height of the cone : ");
	scanf("%f",&height);
	
	//calculate volume of the cone
	volume =  (PI*radius*radius*height)/3;
	printf ("Volume = %f",volume);
	
	return 0;
	
}
