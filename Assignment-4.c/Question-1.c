#include <stdio.h>
int main(){
	
	//Program to calculate the sum and average of any given 3 integers
	int num, i;
	float sum, average;
	
	for(i=0;i<3;i++)
	{
		printf("Enter integer : ");
		scanf("%d",&num);
		
		//calculate sum
		sum+=num;
	}

	
	//calculate average
	average = sum/3;
	
	printf("Sum = %f\n",sum);
	printf("Average = %f",average);
	
    return 0;	
}
